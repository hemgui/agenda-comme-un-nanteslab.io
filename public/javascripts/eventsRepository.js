// events
(function (root) {
    var fetchTimeout = 2000;

    // all this function should be... cleaned
    function clean(event, sources) {
        if ((Array.isArray(event.fbPages))) {
            event.sharers = event.fbPages.map(function (fbPage) {
                if (typeof fbPage.url === 'string') {
                    fbPage.id = fbPage.url.split('/').pop();
                } else {
                    fbPage.id = 'unknown';
                    fbPage.url = '';
                }

                if (typeof fbPage.title !== 'string') {
                    fbPage.title = '-';
                }
                return fbPage;
            });
        } else if (Array.isArray(event.source)) {
            event.sharers = event.source.map(function (source) {
                return {
                    id: source.key,
                    url: source.key,
                    title: source.name
                };
            });
        } else if (Array.isArray(event.sourceKeys)) {
            event.sharers = event.sourceKeys.map(function (source) {
                return {
                    id: source.key || source,
                    url: source.key || source,
                    title: source.libelle || source
                };
            });
        } else {
            event.sharers = [];
        }

        event.sharers = event.sharers.map(function (sharer) {
            if (sources[sharer.id]) {
                return sources[sharer.id];
            }

            return sharer.title;
        });

        event.hosts = (Array.isArray(event.hosts)) ? event.hosts.map(function (host) {
            return (typeof host === 'string') ? host : '';
        }) : [];

        var textfields = ['title', 'description', 'place'];
        textfields.forEach(function (field) {
            if (typeof event[field] !== 'string') {
                event[field] = '';
            }
        });

        var dateFields = ['start', 'end'];
        dateFields.forEach(function (field) {
            if (typeof event[field] !== 'string' || !event[field].match(/20[0-9]{2}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}.*/)) {
                event[field] = '';
            }
        });

        var isValidImage = (Array.isArray(event.image) && event.image.length > 0);
        event.image = isValidImage ? {src: Array.isArray(event.image[0].value) ? event.image[0].value[0] : event.image[0].value} : null;

        return event;
    }

    function fetchEvents(url, sources) {
        return agenda.utils.fetchWithTimeout(url, fetchTimeout)
            .then(function (response) {
                return response.json();
            })
            .then(function (events) {
                return events.map(function(event) {return clean(event, sources);});
            });
    }

    function filterEvents(events, sources) {
        return events.filter(function(event) {
            return (event.fbPages && event.fbPages.some(function(sharer) {
                return sources.includes(sharer.id);
            }))
            || (event.sourceKeys && event.sourceKeys.some(function(sharer) {
                return sources.includes(sharer.key || sharer);
            }));
        });
    }

    var events = {
        fetch: fetchEvents,
        filterEvents: filterEvents
    };

    root.agenda = root.agenda || {};
    root.agenda.events = events;

}(this));
